# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xfwm4-4.4.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require xfce

SUMMARY="XFCE Window Manager"
HOMEPAGE="http://docs.xfce.org/xfce/${PN}/start"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    startup-notification
    xcomposite
"

DEPENDENCIES="
    build+run:
        dev-libs/dbus-glib[>=0.72]
        dev-libs/glib:2[>=2.24]
        gnome-desktop/libwnck:1[>=2.22]
        sys-apps/dbus[>=1.0.0]
        x11-libs/gtk+:2[>=2.14]
        x11-dri/libdrm[>=2.4]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXrandr
        x11-libs/libXrender
        xfce-base/libxfce4ui[>=4.11.0]
        xfce-base/libxfce4util[>=4.8.0]
        xfce-base/xfconf[>=4.8.0]
        startup-notification? ( x11-libs/startup-notification[>=0.5] )
        xcomposite? ( x11-libs/libXcomposite[>=0.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-render
    --enable-xrandr
    --enable-xsync
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    startup-notification
    'xcomposite compositor'
)
